﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoViewButton : MonoBehaviour {


    Image thisButton;
    Text thisButtonsText;
    public GameObject[] slides;
    public string[] Texts;
    public bool shown = false;
    public int slide = 0;
    public int page = 0;
    float AlphaPhase = 0.0f;
    public float autoStart = 0.0f;
	// Use this for initialization
	void Start () {
        thisButton = GetComponent<Image>();
        thisButtonsText = transform.GetChild(0).GetComponent<Text>();
        thisButton.color = new Color(thisButton.color.r, thisButton.color.g, thisButton.color.b, 0);
        thisButtonsText.color = new Color(thisButtonsText.color.r, thisButtonsText.color.g, thisButtonsText.color.b, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (shown && AlphaPhase < 1.0f)
        {
            AlphaPhase += Time.deltaTime;
            if (AlphaPhase > 1.0f)
                AlphaPhase = 1.0f;
            thisButton.color = new Color(thisButton.color.r, thisButton.color.g, thisButton.color.b, AlphaPhase);
            thisButtonsText.color = new Color(thisButtonsText.color.r, thisButtonsText.color.g, thisButtonsText.color.b, AlphaPhase);
        }

        else if (!shown && AlphaPhase > 0.0f)
        {
            AlphaPhase -= Time.deltaTime;
            if (AlphaPhase < 0.0f)
            {
                AlphaPhase = 0.0f;
                this.gameObject.SetActive(false);
            }
            thisButton.color = new Color(thisButton.color.r, thisButton.color.g, thisButton.color.b, AlphaPhase);
            thisButtonsText.color = new Color(thisButtonsText.color.r, thisButtonsText.color.g, thisButtonsText.color.b, AlphaPhase);
        }
    }

    public void showText(string text)
    {
        thisButtonsText.text = text;
        shown = true;
    }

    public void openSlideShow()
    {
        slides[slide].SetActive(true);
      //  slides[slide].transform.GetChild(0).GetComponent<SlideShow>().setSlide(page);
    }
}
