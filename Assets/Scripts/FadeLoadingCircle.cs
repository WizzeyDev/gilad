﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeLoadingCircle : MonoBehaviour {
    public UniversalMediaPlayer ump;
    public GameObject slideScreenContainer;
    public GameObject pointerSphere;
    public string currentTargetPath = "";
    public int currentSlide = 0;
    public int startSlide = 0;
    public int endSlide = 0;
    public float DelayBeforeStart = 3.0f;
    float delay = 0.0f;
    float alpha = 0.0f;
    public bool visible = false;

	// Use this for initialization
	void Start () {
		
	}

    public void openSlideShow(int nextSlide)
    {
        Debug.Log("Activated point: " + currentSlide);
        slideScreenContainer.SetActive(true);
        if (currentTargetPath.Length > 4)
        {
            visible = false;
            if (pointerSphere != null)
                pointerSphere.transform.position = new Vector3(0, 0, 0);
            slideScreenContainer.GetComponent<SlideShow>().setSlide(currentTargetPath, nextSlide);
            slideScreenContainer.GetComponent<SlideShow>().startSlide = startSlide;
            slideScreenContainer.GetComponent<SlideShow>().endSlide = endSlide;
        }
        else
        {
            visible = false;
            if (pointerSphere != null)
                pointerSphere.transform.position = new Vector3(0, 0, 0);
            slideScreenContainer.GetComponent<SlideShow>().setSlide(currentSlide);
            slideScreenContainer.GetComponent<SlideShow>().startSlide = startSlide;
            slideScreenContainer.GetComponent<SlideShow>().endSlide = endSlide;
        }
    }

    // Update is called once per frame
    void Update () {
        if (alpha < 1.0f && visible)
        {
            alpha += Time.deltaTime * 3.0f;
            if (alpha > 1.0f)
            {
                alpha = 1.0f;
            }
            GetComponent<CanvasRenderer>().SetAlpha(alpha);
        }
        else if (alpha > 0.0f && !visible)
        {
            alpha -= Time.deltaTime*3.0f;
            if (alpha < 0.0f)
            {
                alpha = 0.0f;
                delay = 0.0f;
                currentTargetPath = "";
            }
            GetComponent<CanvasRenderer>().SetAlpha(alpha);
        }
        if (delay < DelayBeforeStart && visible)
        {
            delay += Time.deltaTime;
            if (delay > DelayBeforeStart)
            {
                delay = DelayBeforeStart;
                visible = false;
                if (!ump.IsPlaying)
                {
                    openSlideShow(currentSlide);
                }
            }
            GetComponent<Image>().fillAmount = delay / DelayBeforeStart;
        }
    }
}
