﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RawImageFade : MonoBehaviour {
    float targetAlpha = 1.0f;
    public float alpha = 1.0f;
    public CanvasRenderer[] elements;
    // Update is called once per frame
    void Start()
    {
        targetAlpha = alpha;
        setAlpha(targetAlpha);
    }
    void Update () {
        if (targetAlpha < alpha)
        {
            targetAlpha += Time.deltaTime;
            if (targetAlpha > alpha)
            {
                targetAlpha = alpha;
            }
            setAlpha(targetAlpha);
        }
        else if (targetAlpha > alpha)
        {
            targetAlpha -= Time.deltaTime;
            if (targetAlpha < alpha)
            {
                targetAlpha = alpha;
            }
            setAlpha(targetAlpha);
        }
    }

    void setAlpha(float a)
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].SetAlpha(a);
        }
    }
}
