﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerBehaviour : MonoBehaviour {


    public bool targetDetected = false;
    float alphaPhase = 0.0f;
    Text currentText;
	// Use this for initialization
	void Start () {
        currentText = GetComponent<Text>();
        currentText.color = new Color(currentText.color.r, currentText.color.g, currentText.color.b, 0.0f);
    }
	
	// Update is called once per frame
	void Update () {
        if (targetDetected && alphaPhase < 1.0f)
        {
            alphaPhase += Time.deltaTime*2.0f;
            if (alphaPhase > 1.0f)
                alphaPhase = 1.0f;
            currentText.color = new Color(currentText.color.r, currentText.color.g, currentText.color.b, alphaPhase);
        }
        else if(!targetDetected && alphaPhase > 0.0f)
        {
            alphaPhase -= Time.deltaTime * 2.0f;
            if (alphaPhase < 0.0f)
                alphaPhase = 0.0f;
            currentText.color = new Color(currentText.color.r, currentText.color.g, currentText.color.b, alphaPhase);
        }
    }
}
