﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionNoise : MonoBehaviour {

    public float targetChangeFrequency = 1.0f;
    public Vector3 NoisePos = new Vector3(1, 1, 1);
    Vector3 targetPosition;
    Vector3 randomPostitionChange = new Vector3(0, 0, 0);
    float directionChangeInterval = 0.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += targetPosition - transform.position/10.0f;
        //transform.position += 
        if (directionChangeInterval < 1.0f)
            directionChangeInterval += Time.deltaTime * targetChangeFrequency;
        else
        {
            directionChangeInterval = 0.0f;
            randomPostitionChange = new Vector3(Random.Range(-NoisePos.x, NoisePos.x), Random.Range(-NoisePos.y, NoisePos.y), Random.Range(-NoisePos.z, NoisePos.z));
            targetPosition = transform.position + randomPostitionChange;
        }

	}
}
