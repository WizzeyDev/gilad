﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideShow : MonoBehaviour {
    float AlphaPhase = 0.0f;
    public bool visible = false;
    public string[] videoSlidesPaths;
    public int slide = 0;
    public int startSlide = 0;
    public int endSlide = 0;
    public UniversalMediaPlayer ump;
    public RawImage rwImg;
    private bool gameObjectIsActive = false;
    public GameObject[] buttons;// 0 - home, 1 - next, 2 - previous.
    public FocusFix camReference;
    public RawImageFade rawBG;


    // Use this for initialization
    void Start () {
        rwImg.color = new Color(rwImg.color.r, rwImg.color.g, rwImg.color.b, AlphaPhase);
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (visible && AlphaPhase < 1.0f)
        {
            AlphaPhase += Time.deltaTime;
            if (AlphaPhase > 1.0f)
                AlphaPhase = 1.0f;
            rwImg.color = new Color(rwImg.color.r, rwImg.color.g, rwImg.color.b, AlphaPhase);
        }
        else if (!visible && AlphaPhase > 0.0f)
        {
            AlphaPhase -= Time.deltaTime;
            if (AlphaPhase < 0.0f)
            {
                AlphaPhase = 0.0f;
                rwImg.color = new Color(rwImg.color.r, rwImg.color.g, rwImg.color.b, 0);
                ump.Pause();
                for (int i = 0; i < buttons.Length; i++)
                    buttons[i].SetActive(false);
            }
            rwImg.color = new Color(rwImg.color.r, rwImg.color.g, rwImg.color.b, AlphaPhase);
        }

        Debug.Log("Video Length: " + ump.Length / 1000.0f + " Seconds.");
        if (ump.Length > 0 && ump.IsPlaying)
        {
            if (ump.Length - ump.Time < 1000)
            {
                if (slide >= startSlide)
                {
                    ump.Pause();
                }
                else
                {
                    nextSlide(false);
                }
            }
        }
        if (slide <= startSlide)
            buttons[2].SetActive(false);

        if (slide >= endSlide)
            buttons[1].SetActive(false);// 0 - home, 1 - next, 2 - previous.

        if (startSlide == endSlide)
        {
            buttons[2].SetActive(false);
            buttons[1].SetActive(false);
        }


        // the Escape keyCode detected as Back button on Adnroid Mobile devices.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            closeSlideShow();
        }

    }

    public void setSlide(string texturePath, int nextSlide)
    {
        rawBG.alpha = 1.0f;
        camReference.Pause();
        // rwImg.gameObject.SetActive(true);
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].SetActive(true);
        visible = true;
        slide = nextSlide;
        if (slide < 0)
            slide = videoSlidesPaths.Length - 1;
        ump.Stop();
        ump.Path = texturePath;// using SlideKeys as an array of video's paths.
        ump.Play();
    }

    public void playMovie(string path, string nextPreloadedPath, string previousPreloadedPath)
    {

    }

    public void setSlide(int textureNum)
    {
        rawBG.alpha = 1.0f;
        camReference.Pause();
        //  rwImg.gameObject.SetActive(true);
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].SetActive(true);
        visible = true;
        slide = textureNum;
        ump.Stop();
        ump.Path = videoSlidesPaths[textureNum];// using SlideKeys as an array of video's paths.
        ump.Play();
    }

    public void nextSlide(bool manualClick)
    {

            slide = (slide + 1) % videoSlidesPaths.Length;

        if(manualClick && slide < startSlide)
        {
            slide = startSlide;
            setSlide(slide);
        }
        else setSlide(slide);
    }
    bool isVideoPause;

    public void PasueVideo() {
        if (!isVideoPause) {
            ump.Pause();
          
        }
        else {
            ump.Play();
        }
        isVideoPause = !isVideoPause;
    }

    public void previousSlide()
    {
        slide--;
        if (slide < 0)
            slide = videoSlidesPaths.Length - 1;
        setSlide(slide);
    }

    public void closeSlideShow()
    {
        rawBG.alpha = 0.0f;
        camReference.UnPause();
        visible = false;
    }
}
