﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPoint : MonoBehaviour {

    Transform theChild;
    public FadeLoadingCircle fc;
    public string videoPath = "";
    public int slideNum = 0;
    public int slideNumStart = 0;
    public int slideNumEnd = 0;
    public TextMesh PointDescription;
    bool detectionState = false;
    Vector3 initialScale = new Vector3(1, 1, 1);
    float targetPhase = 0.0f;

	// Use this for initialization
	void Start () {
        initialScale = transform.localScale;
        if (PointDescription != null)
            PointDescription.color = new Color(PointDescription.color.r, PointDescription.color.g, PointDescription.color.b, 0.0f);
        theChild = transform.GetChild(0);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, 1.0f+targetPhase*2);
        theChild.transform.Rotate(0, 0, -2.0f- targetPhase*4);
        transform.localScale = initialScale * (1.0f + targetPhase*1.0f);
        if (detectionState && targetPhase < 1.0f)
        {
            targetPhase += Time.deltaTime * 2.0f;
            if (targetPhase > 1.0f)
            {
                    targetPhase = 1.0f;
            }
            if (PointDescription != null)
                PointDescription.color = new Color(PointDescription.color.r, PointDescription.color.g, PointDescription.color.b, targetPhase);
        }
        else if (!detectionState && targetPhase > 0.0f)
        {
            targetPhase -= Time.deltaTime * 2.0f;
            if (targetPhase < 0.0f)
                targetPhase = 0.0f;
            if (PointDescription != null)
                PointDescription.color = new Color(PointDescription.color.r, PointDescription.color.g, PointDescription.color.b, targetPhase);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pointer")
        {
            Debug.Log("Trying to activate point: " + gameObject.name);
            detectionState = true;
            if (fc != null)
            {
                fc.visible = true;
                if (videoPath.Length > 4)
                    fc.currentTargetPath = videoPath;
                fc.currentSlide = slideNum;
                fc.startSlide = slideNumStart;
                fc.endSlide = slideNumEnd;
            }
        }
        Debug.Log("Detected Collsion!");
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "pointer")
        {
            detectionState = false;
            fc.visible = false;
        }
        Debug.Log("Lost Collsion!");
    }

   /* IEnumerator playVideo()
    {
        if (videoPath != "" && videoFade != null)
        {
            GameObject.Find("UniversalMediaPlayer").GetComponent<UniversalMediaPlayer>().Stop();
            GameObject.Find("UniversalMediaPlayer").GetComponent<UniversalMediaPlayer>().RenderingObjects[0] = videoFade.gameObject;
            GameObject.Find("UniversalMediaPlayer").GetComponent<UniversalMediaPlayer>().Path = videoPath;
            GameObject.Find("UniversalMediaPlayer").GetComponent<UniversalMediaPlayer>().Play();
        }
    }*/
}
