﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoviePreloader : MonoBehaviour {

    public string[] preloadPath;
    public UniversalMediaPlayer[] mediaPlayers;
    public CanvasRenderer[] renderedElementLayer;
    int activeLayer = -1;
    float[] alpha;

	// Use this for initialization
	void Start () {
        alpha = new float[preloadPath.Length];
        for (int i = 0; i < preloadPath.Length; i++)
            alpha[i] = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
      /*for (int i = 0; i < preloadPath.Length; i++)
        {
            if (mediaPlayers[i].IsPlaying && i != activeLayer)
            mediaPlayers[i].Pause();

        }*/

    }

    void preloadAllPaths()
    {
        for (int i = 0; i < preloadPath.Length; i++)
        {
            mediaPlayers[i].Path = preloadPath[i];
            mediaPlayers[i].Mute = true;
            mediaPlayers[i].Play();
        }
    }

    void LerpAlpha(float TargetAlpha, int layer)
    {
        if (alpha[layer] < TargetAlpha)
        {
            alpha[layer] += Time.deltaTime;
            if (alpha[layer] > TargetAlpha)
                alpha[layer] = TargetAlpha;
            renderedElementLayer[layer].SetAlpha(alpha[layer]);
        }
        if (alpha[layer] > TargetAlpha)
        {
            alpha[layer] -= Time.deltaTime;
            if (alpha[layer] < TargetAlpha)
                alpha[layer] = TargetAlpha;
            renderedElementLayer[layer].SetAlpha(alpha[layer]);
        }
    }
}
