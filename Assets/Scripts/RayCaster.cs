﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCaster : MonoBehaviour {

    public GameObject collsiionPoint;
    RaycastHit hit;
    Ray ray;
    /*LayerMask layerMask = 1;*/
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        ray = GetComponent<Camera>().ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2));
        Physics.Raycast(ray, out hit);
        if (hit.collider != null)
            collsiionPoint.transform.position = new Vector3(hit.point.x, 0, hit.point.z);
        else collsiionPoint.transform.position = new Vector3(-10, -10, -10);
        Debug.DrawLine(ray.origin, new Vector3(hit.point.x, 0, hit.point.z));
    }
}
