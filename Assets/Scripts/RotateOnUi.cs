﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnUi : MonoBehaviour {

    public Vector3 rotateEachFrame = new Vector3(0, 0, 0);
	// Update is called once per frame
	void Update () {
        transform.Rotate(rotateEachFrame);
	}
}
